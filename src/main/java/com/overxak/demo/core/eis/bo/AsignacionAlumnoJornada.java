package com.overxak.demo.core.eis.bo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
@Entity
@Table(name="asignacion_alumno_jornada")
@Data
public class AsignacionAlumnoJornada implements Serializable {
    @EmbeddedId
    private AlumnoAsignacionId alumnoAsignacionId;
    @ManyToOne
    @JoinColumn(name="carne",referencedColumnName="carne",nullable=false,insertable=false,updatable=false)
    private Alumno alumno;
    @ManyToOne
    @JoinColumn(name="codigo_jornada",referencedColumnName="codigo_jornada",nullable=false,insertable=false,updatable=false)
    private Jornada jornada;
    @Column(name="fecha_asignacion_jornada")
    private Date fechaAsignacionJornada;
    @Column(name="comentario")
    private String comentario;
    @Column(name="status")
    private String status;
    @Column(name="codigo_carrera")
    private Integer codigoCarrera;
    @Column(name="comentarios")
    private String comentarios;
}
