package com.overxak.demo.core.bs.dao;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.overxak.demo.core.eis.bo.Carrera;

public interface CarreraRepository extends PagingAndSortingRepository<Carrera, Integer> {

}
