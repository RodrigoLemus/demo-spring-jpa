package com.overxak.demo.core.eis.bo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="profesor")
public class Profesor implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_profesor")
    private Integer codigoProfesor;
    @Column(name="nombre_profesor")
    private String nombreProfesor;
    @Column(name="direccion")
    private String direccion;
    @Column(name="telefono")
    private Integer telefono;
    @Column(name="comentario")
    private String comentario;
    @Column(name="status_persona")
    private String statusPersona;
    @Column(name="area_profesor")
    private String areaProfesor;
}
