package com.overxak.demo.core;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import com.overxak.demo.core.bs.dao.CarreraRepository;
import com.overxak.demo.core.eis.bo.Carrera;

@Controller
public class MainController {
    @Autowired
    private CarreraRepository carreraRepository;
    @RequestMapping("/")
    @ResponseBody
    public String Index() {
        String response = "Iniciando el Proyecto";
        Carrera carrera = new Carrera();
        carrera.setNombreCarrera("Informática");
        carreraRepository.save(carrera);
        return response;
    }
}
