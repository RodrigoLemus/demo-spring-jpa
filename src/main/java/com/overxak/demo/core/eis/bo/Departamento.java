package com.overxak.demo.core.eis.bo;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="departamento")
public class Departamento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_departamento")
    private Integer codigoDepartamento;
    @Column(name="nombre")
    private String nombre;
    @OneToMany(mappedBy="departamento")
    private Set<Departamento> departamento;
}
