package com.overxak.demo.core.eis.bo;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="carrera")
public class Carrera implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="codigo_carrera")
    private Integer codigoCarrera;
    @Column(name="nombre_carrera")
    private String nombreCarrera;
    @Column(name="status")
    private String status;
}
